### How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__
- That's it - load the homepage, use __Register__ link and use the CRUD

---
Note: This repo is forking from : https://github.com/LaravelDaily/Laravel-Vue-First-CRUD