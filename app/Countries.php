<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $fillable = [
    	'name', 
    	'created_date', 
    	'created_by', 
    	'updated_date', 
    	'updated_by'
   	];

   	public function films()
    {
        return $this->hasMany('App\Films');
    }
}
