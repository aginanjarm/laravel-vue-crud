<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    protected $fillable = [
    	'name', 
    	'description', 
    	'created_date', 
    	'created_by', 
    	'updated_date', 
    	'updated_by'
   	];

    public function films()
    {
        return $this->hasMany('App\Films');
    }
}
