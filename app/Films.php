<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Films extends Model
{
    protected $table = 'films';

    public $timestamps = false;

    protected $fillable = [
    	'name', 
    	'description', 
    	'release_date', 
    	'rating', 
    	'ticket_price', 
    	'country_id', 
        'genre_id', 
    	'photo', 
    	'created_date', 
    	'created_by', 
    	'updated_date', 
    	'updated_by'
   	];

    public static function getAll() 
    {
        try{
            $films = DB::table('films')
                ->select(
                    'films.id',
                    'films.name',
                    'films.description',
                    'release_date',
                    'rating',
                    'ticket_price',
                    'country_id',
                    'countries.name as country_name',
                    'genre_id',
                    'genres.name as genre_name',
                    'photo',
                    'films.created_date',
                    'films.created_by',
                    'films.updated_date',
                    'films.updated_by')
                ->join('countries', 'countries.id', '=',
                            'films.country_id')
                ->join('genres', 'genres.id', '=',
                            'films.genre_id')
                ->get();
            return $films;
        } catch(Exception $e) {
            print($e->getMessage());die;
            return [];
        }
    }

    public function country()
    {
        return $this->belongsTo('App\Countries');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genres');
    }
}
