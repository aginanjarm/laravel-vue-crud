<?php

namespace App\Http\Controllers\Api\V1;

use App\Genres;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GenresController extends Controller
{
    public function index()
    {
        return Genres::all();
    }

    public function show($id)
    {
        return Genres::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $genres = Genres::findOrFail($id);
        $genres->update($request->all());

        return $genres;
    }

    public function store(Request $request)
    {
        $genres = Genres::create($request->all());
        return $genres;
    }

    public function destroy($id)
    {
        $genres = Genres::findOrFail($id);
        $genres->delete();
        return '';
    }
}
