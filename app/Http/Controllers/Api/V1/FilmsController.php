<?php

namespace App\Http\Controllers\Api\V1;

use App\Films;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilmsController extends Controller
{
    public function index()
    {
        $films = Films::getAll();

        return $films;
    }

    public function show($id)
    {
        return Films::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $Films = Films::findOrFail($id);
        $Films->update($request->all());

        return $Films;
    }

    public function store(Request $request)
    {
        $data = $request->all();

        // dd($request->all());die;
        $exploded = explode(',', $request->photo);
        $decoded = base64_decode($exploded[1]);
        if(str_contains($exploded[0], 'jpeg'))
        $extention = 'jpg';
        elseif(str_contains($exploded[0], 'gif'))
        $extention = 'gif';
        else
        $extention = 'png';

        $fileName = str_random() .'.'. $extention;
        
        $data['photo'] = $fileName;
        $data['created_date'] = time();
        $data['created_by'] = 1;
        $data['updated_date'] = time();
        $data['updated_by'] = 1;

        $Films = Films::create($data);

        $path = public_path() . '/images/' . $fileName;
        file_put_contents($path, $decoded);
        return $Films;
    }

    public function destroy($id)
    {
        $Films = Films::findOrFail($id);
        $Films->delete();
        return '';
    }
}
