<?php

namespace App\Http\Controllers\Api\V1;

use App\Countries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
    public function index()
    {
        return Countries::all();
    }

    public function show($id)
    {
        return Countries::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $countries = Countries::findOrFail($id);
        $countries->update($request->all());

        return $countries;
    }

    public function store(Request $request)
    {
        $countries = Countries::create($request->all());
        return $countries;
    }

    public function destroy($id)
    {
        $countries = Countries::findOrFail($id);
        $countries->delete();
        return '';
    }
}
