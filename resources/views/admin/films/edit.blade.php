@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add new film</div>

                    <div class="panel-body table-responsive">
                        {!! Form::model($company, ['method' => 'PUT', 'route' => ['admin.films.update', $company->id]]) !!}

                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('name'))
                                    <p class="help-block">
                                        {{ $errors->first('name') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                                {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('description'))
                                    <p class="help-block">
                                        {{ $errors->first('description') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('release_date', 'Release Date', ['class' => 'control-label']) !!}
                                {!! Form::text('release_date', old('release_date'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('release_date'))
                                    <p class="help-block">
                                        {{ $errors->first('release_date') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('ticket_price', 'Ticket Price', ['class' => 'control-label']) !!}
                                {!! Form::text('ticket_price', old('ticket_price'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('ticket_price'))
                                    <p class="help-block">
                                        {{ $errors->first('ticket_price') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('country_id', 'Country', ['class' => 'control-label']) !!}
                                {!! Form::text('country_id', old('country_id'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('country_id'))
                                    <p class="help-block">
                                        {{ $errors->first('country_id') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 form-group">
                                {!! Form::label('genre_id', 'Genre', ['class' => 'control-label']) !!}
                                {!! Form::text('genre_id', old('genre_id'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('genre_id'))
                                    <p class="help-block">
                                        {{ $errors->first('genre_id') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

