<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    Route::resource('companies', 'CompaniesController', ['except' => ['create', 'edit']]);
    Route::resource('films', 'FilmsController', ['except' => ['create', 'edit']]);
    Route::resource('genres', 'GenresController', ['except' => ['create', 'edit']]);
    Route::resource('countries', 'CountriesController', ['except' => ['create', 'edit']]);
});
