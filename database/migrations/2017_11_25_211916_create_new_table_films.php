<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTableFilms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->bigInteger('release_date')->default(0);
            $table->tinyInteger('rating')->default(0);
            $table->unsignedDecimal('ticket_price',18,2)->default(0);
            $table->integer('country_id')->default(0);
            $table->integer('genre_id')->default(0);
            $table->text('photo');
            $table->bigInteger('created_date')->default(0);
            $table->integer('created_by')->default(0);
            $table->bigInteger('updated_date')->default(0);
            $table->integer('updated_by')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
