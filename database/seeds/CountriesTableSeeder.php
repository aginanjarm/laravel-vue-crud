<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$list = [
    		[
    			'name'=>'America'
    		],[
    			'name'=>'British'
    		],[
    			'name'=>'Indonesia'
    		],[
    			'name'=>'Turkey'
    		],[
    			'name'=>'Saudi Arabia'
    		],[
    			'name'=>'Rusia'
    		]
    	];
    	\App\Countries::insert($list);
    }
}
