<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$list = [
    		[
    			'name'=>'Comedy',
                'description'=>''
    		],[
    			'name'=>'Drama',
                'description'=>''
    		],[
    			'name'=>'Anime',
                'description'=>''
    		],[
    			'name'=>'Cartoon',
                'description'=>''
    		],[
    			'name'=>'Family',
                'description'=>''
    		],[
    			'name'=>'Mystery',
                'description'=>''
    		]
    	];
    	\App\Genres::insert($list);
    }
}
